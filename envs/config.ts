
import {config as configDev} from './config-dev';
import {config as configProd} from './config-prod';

const mode = {
    'dev': true,
    'prod': false
};

let config = configDev;
if (mode.dev) {
    config = configDev;
}

if (mode.prod) {
    this.config = configProd;
}

export {config};