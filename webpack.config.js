const path = require('path');
const nodeExternals = require('webpack-node-externals');

module.exports = {
    mode: 'development',
    module: {
        rules: [
          {
            test: /\.ts?$/,
            use: 'ts-loader',
            exclude: /node_modules/
          }
        ]
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js']
    },
    externals: [nodeExternals()],
    entry: {
        'server': './src/server.ts'
    },
    output: {
      path: path.join(__dirname, 'dist'),
      filename: '[name].js',
    }
  };