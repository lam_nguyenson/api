import * as _ from "lodash";
import * as http from 'http';
import * as debug from 'debug';
import app from './app';
import {config} from '../envs/config';

debug('ts-express:server');

const PORT = normalizePort(config.PORT || 3000);
app.set('port', PORT);

const server = http.createServer(app);
server.listen(PORT);
server.on('error', onError);
server.on('listening', onListening);

function normalizePort(val: number|string): number|string|boolean {
    let port: number = _.isString(val) ? _.parseInt(val) : val;
    if(isNaN(port)) return val;
    else if ( port >= 0 ) return port;
    else return false;
}

function onError(error: NodeJS.ErrnoException): void {
    if(error.syscall !== 'listen') throw error;
    let bind = _.isString(PORT) ? 'Pipe ' + PORT : 'Port '+ PORT;
    switch (error.code) {
        case 'EACCES':
            console.error(`${bind} requires elevated privileges`);
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(`${bind} is already in use`);
            process.exit(1);
            break;
        default:
            throw error;
    }
}

function onListening(): void {
    let addr = server.address();
    let bind = _.isString(addr) ? `pipe ${addr}` : `port ${addr.port}`;
    debug(`Listening on ${bind}`);
}
