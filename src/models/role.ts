import {Document, Model, model, Schema} from 'mongoose';

export interface Role extends Document {
    name: string;
    permissions: object[];
    status: boolean;
    updated_at?: Date;
    created_at?: Date;
}

let RoleSchema: Schema = new Schema({
    _id: Schema.Types.ObjectId,
    name: {type: String, required: true},
    permissions: {type: [Object], required: false},
    status: {type: Boolean, required: true},
    updated_at: {type: Date, required: false},
    created_at: {type: Date, required: false, default: new Date()}
});


export const Roles: Model<Role> = model<Role>('roles', RoleSchema);