export const DataPermissions = [
	{
		name: 'View dashboard',
		path: '/dashboard',
		method: 'GET'
	},
	{
		name: 'Get Permission list',
		path: '/permissions',
		method: 'GET'
	},
	{
		name: 'Get Permission ID',
		path: '/permissions/:permission_id',
		method: 'GET'
	},
	{
		name: 'Add new permission',
		path: '/permissions',
		method: 'POST'
	},
	{
		name: 'Update permission',
		path: '/permissions/:permission_id',
		method: 'PUT'
	},
	{
		name: 'Delete permission',
		path: '/permissions/:permission_id',
		method: 'DELETE'
	},
	{
		name: 'Get Users list',
		path: '/users',
		method: 'GET'
	},
	{
		name: 'Get User ID',
		path: '/users/:user_id',
		method: 'GET'
	},
	{
		name: 'Add new user',
		path: '/users',
		method: 'POST'
	},
	{
		name: 'Update user',
		path: '/users/:user_id',
		method: 'PUT'
	},
	{
		name: 'Delete user',
		path: '/users/:user_id',
		method: 'DELETE'
	},
	{
		name: 'Get Role list',
		path: '/roles',
		method: 'GET'
	},
	{
		name: 'Get Role ID',
		path: '/roles/:role_id',
		method: 'GET'
	},
	{
		name: 'Add new role',
		path: '/roles',
		method: 'POST'
	},
	{
		name: 'Update role',
		path: '/roles/:role_id',
		method: 'PUT'
	},
	{
		name: 'Delete role',
		path: '/roles/:role_id',
		method: 'DELETE'
	},
	{
		name: 'Get Posts list',
		path: '/posts',
		method: 'GET'
	},
	{
		name: 'Get Post ID',
		path: '/posts/:post_id',
		method: 'GET'
	},
	{
		name: 'Add new post',
		path: '/posts',
		method: 'POST'
	},
	{
		name: 'Update post',
		path: '/posts/:post_id',
		method: 'PUT'
	},
	{
		name: 'Delete post',
		path: '/posts/:post_id',
		method: 'DELETE'
	},
	{
		name: 'Get Comments list',
		path: '/comments',
		method: 'GET'
	},
	{
		name: 'Get Comment ID',
		path: '/comments/:comment_id',
		method: 'GET'
	},
	{
		name: 'Add new Comment',
		path: '/comments',
		method: 'POST'
	},
	{
		name: 'Update Comment',
		path: '/comments/:comment_id',
		method: 'PUT'
	},
	{
		name: 'Delete Comment',
		path: '/comments/:comment_id',
		method: 'DELETE'
	},
	{
		name: 'Get Media list',
		path: '/comments',
		method: 'GET'
	},
	{
		name: 'Get Comment ID',
		path: '/medias/:media_id',
		method: 'GET'
	},
	{
		name: 'Upload media',
		path: '/medias/upload',
		method: 'POST'
	},
	{
		name: 'Upload multi media',
		path: '/medias/uploads',
		method: 'POST'
	},
	{
		name: 'Update Media',
		path: '/medias/:media_id',
		method: 'PUT'
	},
	{
		name: 'Delete Media',
		path: '/medias/:media_id',
		method: 'DELETE'
	},
];