import {Document, Model, model, Schema} from 'mongoose';

export interface Permission extends Document {
    name: string;
    path: string;
    method: string;
    updated_at?: Date;
    created_at?: Date;
}

let PermissionSchema: Schema = new Schema({
    _id: Schema.Types.ObjectId,
    name: {type: String, required: true},
    path: {type: String, required: true},
    method: {type: String, required: true},
    updated_at: {type: Date, required: false},
    created_at: {type: Date, required: false, default: new Date()}
});

export const Permissions: Model<Permission> = model<Permission>('permissions', PermissionSchema);