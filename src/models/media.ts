import {Document, Model, model, Schema} from 'mongoose';

export enum TypeMedia {
    Image = 'image',
    Audio = 'audio',
    Video = 'video',
    Others = 'others'
}
export interface Media extends Document {
    user_id?: string;
    original_name?: string;
    file_name?: string;
    encoding?: string;
    mimetype?: string;
    path?: string;
    url?: string;
    type?: TypeMedia;
    size?: {
        width: number,
        height: number
    };
    created_at?: Date;
    updated_at?: Date;
}

let MediaSchema: Schema = new Schema({
    _id: Schema.Types.ObjectId,
    user_id: {type: String, required: false},
    original_name: {type: String, required: false},
    file_name: {type: String, required: false},
    encoding: {type: String, required: false},
    mimetype: {type: String, required: false},
    path: {type: String, required: false},
    url: {type: String, required: false},
    type: {type: String, required: false},
    size: {type: Object, required: false},
    created_at: {type: Date, required: false, default: new Date()},
    updated_at: {type: Date, required: false}
});


export const Medias: Model<Media> = model<Media>('media', MediaSchema);