import {Document, Model, model, Schema} from 'mongoose';

export interface Post extends Document {
    user?: {
        user_id: string,
        display_name: string
    };
    comment_ids?: string[];
    cats?: {
        name: string,
        slug: string,
        url: string,
        image_feature?: string,
        order?: string
    }[];
    name?: string;
    title?: string;
    content: string;
    image_features?: {
        name: string,
        url: string
    };
    image_gallerys?: string[];
    excerpt?: string;
    feature?: string;
    slug?: string;
    tags?: string[];
    status?: boolean;
    created_at?: Date;
    updated_at?: Date;
}

let PostSchema: Schema = new Schema({
    _id: Schema.Types.ObjectId,
    user: {
        user_id: {type: String, required: true},
        display_name: {type: String, required: true}
    },
    comment_ids: {type: [String], required: false},
    cats: [{
        name: {type: String, required: true},
        slug: {type: String, required: true},
        url: {type: String, required: true},
        image_feature: {type: String, required: false},
        order: {type: String, required: false}
    }],
    name: {type: String, required: false},
    title: {type: String, required: false},
    content: {type: String, required: true},
    image_features: {
        name: {type: String, required: false},
        url: {type: String, required: false}
    },
    image_gallerys: {type: [String], required: false},
    excerpt: {type: String, required: false},
    feature: {type: Boolean, required: false},
    slug: {type: String, required: false},
    tags: {type: [String], required: false},
    status: {type: Boolean, required: false},
    created_at: {type: Date, required: false, default: new Date()},
    updated_at: {type: Date, required: false},
});


export const Posts: Model<Post> = model<Post>('posts', PostSchema);