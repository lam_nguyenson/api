import {Document, Model, model, Schema} from 'mongoose';

export interface Comment extends Document {
    post_id: string;
    author?: string;
    author_email?: string;
    content?: string;
    approved?: Boolean;
    type?: string;
    parent?: string;
    created_at?: Date;
    updated_at?: Date;
}

let CommentSchema: Schema = new Schema({
    _id: Schema.Types.ObjectId,
    post_id: {type: String, required: false},
    author: {type: String, required: false},
    author_email: {type: String, required: false},
    content: {type: String, required: false},
    approved: {type: Boolean, required: false},
    type: {type: String, required: false},
    parent: {type: String, required: false},
    created_at: {type: Date, required: false, default: new Date()},
    updated_at: {type: Date, required: false}
});

export const Comments: Model<Comment> = model<Comment>('comments', CommentSchema);