import {Document, Model, model, Schema} from 'mongoose';

export interface Admin extends Document {
    first_name?: string;
    last_name?: string;
    username?: string;
    email: string;
    password: string;
    role: string;
    display_name?: string,
    company?: string;
    address?: string;
    phone?: string;
    avatar?: string;
    status: boolean;
    updated_at?: Date;
    created_at?: Date;
}

let AdminSchema: Schema = new Schema({
    _id: Schema.Types.ObjectId,
    first_name: {type: String, required: false},
    last_name: {type: String, required: false},
    username: {type: String, required: true, unique: true},
    email: {type: String, required: true, unique: true},
    password: {type: String, required: true},
    role: {type: String, required: true},
    display_name: {type: String, required: false},
    company: {type: String, required: false},
    address: {type: String, required: false},
    phone: {type: String, required: false},
    avatar: {type: String, required: false},
    status: {type: Boolean, required: true},
    updated_at: {type: Date, required: false},
    created_at: {type: Date, required: false, default: new Date()}
});

export const Admins: Model<Admin> = model<Admin>('admin', AdminSchema);