import * as _ from "lodash";
import {Permissions} from '../models/permission';
import {DataPermissions} from '../models/seed/permissions';
import {Roles} from '../models/role';
import {DataRoles} from '../models/seed/roles';
const fs = require('fs');


export function seedData() {
    Permissions.find({}).then((result: any) => {
        if (_.isEmpty(result)) {
            const permissions = DataPermissions;
            Permissions.insertMany(permissions).then((result) => {
                console.log("Seed Permission Successfully!!!");
            }).catch((err) => {
                return err;
            })
        }
    });

    Roles.find({}).then((result: any) => {
        if (_.isEmpty(result)) {
            const roles = DataRoles;
            Roles.insertMany(roles).then((result) => {
                console.log('Seed Roles Successfully!!!');
            }).catch((err) => {
                return err;
            })
        }
    });
}

export function processQuery(params: any) {
    const query = {
        email: '',
        name: '',
        username: '',
        post_id: '',
        title: '',
        'user.user_id': '',
        'cats.name': '',
        permission: '',
        status: true,
        created_at: {
            $gte: '',
            $lte: ''
        }
    };
    // query
    params['email'] ? (query.email = params['email']) : delete query.email;
    params['name'] ? (query.name = params['name']) : delete query.name;
    params['username'] ? (query.username = params['username']) : delete query.username;
    params['post_id'] ? (query.post_id = params['post_id']) : delete query.post_id;

    params['title'] ? (query.title = params['title']) : delete query.title;
    params['user_id'] ? (query['user.user_id'] = params['user_id']) : delete query['user.user_id'];
    params['cat_name'] ? (query['cats.name'] = params['cat_name']) : delete query['cats.name'];

    params['status'] ? (query.status = params['status']) : delete query.status;
    params['start_time'] ? (query.created_at['$gte'] = params['start_time']) : delete query.created_at['$gte'];
    params['end_time'] ? (query.created_at['$lte'] = params['end_time']) : delete query.created_at['$lte'];
    _.isEmpty(query.created_at) ? delete query.created_at : false;

    return query;

}

export function processOptions(params: any): {
    sort: object,
    skip: number,
    limit: number
} {
    const options = {
        sort: {},
        skip:  0,
        limit: 100
    };

    // Options
    const sort = params['sort'] ? params['sort'] : '';
    sort ? (options.sort = {[sort]: -1}) : (options.sort = {created_at: -1});
    params['skip'] ? (options.skip = 0) : false;
    params['limit'] ? (options.limit = 100) : false;

    return options;
}