import * as _ from "lodash";
import {TypeMedia} from '../models/media';
import * as path from 'path';

const multer = require('multer');
const fs = require('fs');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        let path =  __dirname + '/../../uploads';
        if (file && file.mimetype && _.includes(file.mimetype, TypeMedia.Image)) {
            path = path + '/images';
            file['type'] = TypeMedia.Image;
        } else if (file && file.mimetype && _.includes(file.mimetype, TypeMedia.Audio)) {
            path = path + '/audios';
            file['type'] = TypeMedia.Audio;
        } else if (file && file.mimetype && _.includes(file.mimetype, TypeMedia.Video)) {
            path = path + '/videos';
            file['type'] = TypeMedia.Video;
        } else {
            path = path + '/others';
            file['type'] = TypeMedia.Others;
        }
        if (!fs.existsSync(path)) {
            fs.mkdirSync(path);
        }
        cb(null,path);
    },
    filename: (req, file, cb) => {
        cb(null, file.originalname);
    }
});
const UploadMedia = multer({storage: storage});

function formatNameImageResize(input: string, width: number, height: number): string {
    if (input) {
        const nameImageExt = path.basename(input);
        if (nameImageExt) {
            const nameImage = (_.split(nameImageExt, '.')).slice(0, -1).join('.');
            if (nameImage) {
                return _.replace(input, nameImage, nameImage + '-'
                    + width + 'x' + height);
            }
        }
    }

    return '';
}

export  {UploadMedia, formatNameImageResize};