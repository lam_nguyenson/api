import {Request, Response, NextFunction} from "express";
import { config } from "../../envs/config";
const jwt = require("jsonwebtoken");

export class CheckAuth {
    public checkToken(req: Request, res: Response, next: NextFunction) {
        try {
            const token = req.headers.authorization.split(" ")[1];
            const decoded = jwt.verify(token, config.JWT_KEY);
            req['userData'] = decoded;
            next();
        } catch (error) {
            return res.status(401).json({
                message: 'No token provided!'
            });
        }
    }

    public checkPermission(req: Request, res: Response, next: NextFunction) {
        try {
            let path = req.route.path;
            let method = req.method;
            let pers = req['userData'].permissions.filter(per => per.method === method && per.path === path);
            if(req['userData'].permissions && pers.length > 0) {
                next();
            } else {
                return res.status(403).json({
                    message: 'Unauthorized access'
                });
            }
        } catch {
            return res.status(403).json({
                message: 'Unauthorized access'
            });
        }
    }
}