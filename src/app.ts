import * as _ from "lodash";
import * as express from "express";
// import * as cookieParser from 'cookie-parser';
import {Request, Response, NextFunction, ErrorRequestHandler} from "express";
import * as logger from 'morgan';
import * as bodyParser from "body-parser";
import * as cors from 'cors';
import * as helmet from 'helmet';
import * as mongoose from "mongoose";
import {config} from '../envs/config';
import { AdminRouter } from './routes/router-admin';
import * as path from 'path';
import {seedData} from './core/utils';
// var cookieParser = require('cookie-parser');

class App {
    public app: express.Application;
    public adminRoute: AdminRouter = new AdminRouter();
    public mongoUrl: string = config.urlDB;

    constructor() {
        this.app = express();
        this.config();
        this.mongoSetup();
        this.routes();
    }

    private config(): void{
        // this.app.use(cookieParser());
        this.app.use(logger('dev'));
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: false }));
        this.app.use(helmet());
        this.app.use(cors());
        // serving static files
        this.app.use('/uploads', express.static('uploads'));

        // Cors
        this.app.use((req: Request, res: Response, next: NextFunction) => {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
            res.header(
              "Access-Control-Allow-Headers",
              "Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Allow-Credentials"
            );
            res.header("Access-Control-Allow-Credentials", "true");
            next();
        });
    }

    private mongoSetup(): void{
        // mongoose.Promise = global.Promise;
        mongoose.connect(this.mongoUrl, { useNewUrlParser: true });
        mongoose.set('useCreateIndex', true);
        seedData();
    }

    private routes(): void {
        let router: express.Router = express.Router();
        this.app.use('/', router);
        this.app.use(`${config.NAME_ROUTER}`, this.adminRoute.router);
    }
}

export default new App().app;