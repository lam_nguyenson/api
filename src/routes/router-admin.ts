///<reference path="../core/MediaUpload.ts"/>
import {Router, Request, Response, NextFunction} from 'express';
import {CheckAuth} from './../middleware/check-auth';
import {PermissionController} from './../controllers/PermissionController';
import {RolesController} from './../controllers/RolesController';
import {UserController} from './../controllers/UserController';
import {PostController} from '../controllers/PostController';
import {CommentController} from '../controllers/CommentController';
import {MediaController} from '../controllers/MediaController';
import {UploadMedia} from '../core/MediaUpload';
import {AdminController} from '../controllers/AdminController';
import {seedData} from '../core/utils';


export class AdminRouter {
    public router: Router;
    public checkAuth = new CheckAuth();
    public PermissionController = new PermissionController();
    public RolesController = new RolesController();
    public UserController = new UserController();
    public AdminController = new AdminController();
    public PostController = new PostController();
    public CommentController = new CommentController();
    public MediaController = new MediaController();

    public asyncMiddleware = fn => (req: Request, res: Response, next: NextFunction) => {
        Promise.resolve(fn(req, res, next)).catch(next);
    };


    constructor() {
        this.router = Router();
        this.router.post('/login_user', this.asyncMiddleware(this.UserController.login));
        this.router.post('/login_admin', this.asyncMiddleware(this.AdminController.login));
        this.router.use(this.checkAuth.checkToken);
        this.routes();
    }

    public routes(): void {

        /** Permission **/
        this.router.get('/permissions', this.checkAuth.checkPermission, this.PermissionController.getPermissions);
        /** Roles **/
        this.router.get('/roles', this.checkAuth.checkPermission, this.asyncMiddleware(this.RolesController.gets));
        this.router.get('/roles/:role_id', this.checkAuth.checkPermission, this.asyncMiddleware(this.RolesController.getById));
        this.router.post('/roles', this.checkAuth.checkPermission, this.asyncMiddleware(this.RolesController.add));
        this.router.put('/roles/:role_id', this.checkAuth.checkPermission, this.asyncMiddleware(this.RolesController.edit));
        this.router.delete('/roles/:role_id', this.checkAuth.checkPermission, this.asyncMiddleware(this.RolesController.delete));

        /** Admins **/
        this.router.get('/admins/counts', this.checkAuth.checkPermission, this.asyncMiddleware(this.AdminController.counts));
        this.router.get('/admins', this.checkAuth.checkPermission, this.asyncMiddleware(this.AdminController.gets));
        this.router.get('/admins/:admin_id', this.asyncMiddleware(this.AdminController.getById));
        this.router.post('/admins', this.asyncMiddleware(this.AdminController.add));
        this.router.put('/admins/:admin_id', this.asyncMiddleware(this.AdminController.edit));
        this.router.delete('/admins/:admin_id', this.asyncMiddleware(this.AdminController.delete));


        /** Users **/
        this.router.get('/users/counts', this.checkAuth.checkPermission, this.asyncMiddleware(this.UserController.counts));
        this.router.get('/users', this.checkAuth.checkPermission, this.asyncMiddleware(this.UserController.gets));
        this.router.get('/users/:user_id', this.asyncMiddleware(this.UserController.getById));
        this.router.post('/users', this.asyncMiddleware(this.UserController.add));
        this.router.put('/users/:user_id', this.asyncMiddleware(this.UserController.edit));
        this.router.delete('/users/:user_id', this.asyncMiddleware(this.UserController.delete));

        /** Posts **/
        this.router.get('/posts/counts', this.checkAuth.checkPermission, this.asyncMiddleware(this.PostController.counts));
        this.router.get('/posts', this.checkAuth.checkPermission, this.asyncMiddleware(this.PostController.gets));
        this.router.get('/posts/:post_id', this.checkAuth.checkPermission, this.asyncMiddleware(this.PostController.getById));
        this.router.post('/posts', this.checkAuth.checkPermission, this.asyncMiddleware(this.PostController.add));
        this.router.put('/posts/:post_id', this.checkAuth.checkPermission, this.asyncMiddleware(this.PostController.edit));
        this.router.delete('/posts/:post_id', this.checkAuth.checkPermission, this.asyncMiddleware(this.PostController.delete));

        /** Comments **/
        this.router.get('/comments/counts', this.checkAuth.checkPermission, this.asyncMiddleware(this.CommentController.counts));
        this.router.get('/comments', this.checkAuth.checkPermission, this.asyncMiddleware(this.CommentController.gets));
        this.router.get('/comments/:comment_id', this.checkAuth.checkPermission, this.asyncMiddleware(this.CommentController.getById));
        this.router.post('/comments', this.checkAuth.checkPermission, this.asyncMiddleware(this.CommentController.add));
        this.router.put('/comments/:comment_id', this.checkAuth.checkPermission, this.asyncMiddleware(this.CommentController.edit));
        this.router.delete('/comments/:comment_id', this.checkAuth.checkPermission, this.asyncMiddleware(this.CommentController.delete));

        /** Media **/
        this.router.get('/medias', this.MediaController.getQueries);
        this.router.get('/medias/:media_id', this.MediaController.getById);
        this.router.post('/medias/upload', UploadMedia.single('media'), this.MediaController.upload);
        this.router.post('/medias/uploads', UploadMedia.array('medias', 100), this.MediaController.uploads);
        this.router.put('/medias/:media_id', this.MediaController.edit);
        this.router.delete('/medias/:media_id', this.MediaController.delete);

    }

    public dataAction(req: Request, res: Response, next: NextFunction) {
        console.log('AAAAAAAa');
        // console.log('STATUS' + res.status);
    }

}

const adminRoutes = new AdminRouter();
adminRoutes.routes();

export default adminRoutes.router;