import * as _ from "lodash";
import { Request, Response} from 'express';
import * as bcrypt from 'bcrypt';
import * as jwt from 'jsonwebtoken';
import * as mongoose from 'mongoose';
import { Roles } from './../models/role';
import { config } from './../../envs/config';
import {Admins} from '../models/admin';
import {processOptions, processQuery} from '../core/utils';

export class AdminController {
    public async add(req: Request, res: Response) {
        const body = req['body'] ? req['body'] : {};
        if(!body.email || !body.password) {
            res.status(400).json({
                message: (!body.email) ? 'Email invalid' : 'Password invalid'
            });
        }
        const admin = await Admins.findOne({$or: [{email: body.email}, {username: body.username}]});
        if(!_.isEmpty(admin)) {
            return res.status(409).json({
                message: 'Mail exits'
            });
        }

        bcrypt.hash(body.password, 10, async (err, hash) => {
            if(err) {
                return res.status(500).json({
                    message: 'Interal server error'
                });
            }
            const admin = new Admins({
                _id: new mongoose.Types.ObjectId(),
                email: body.email,
                password: hash,
                role: (body.role) ? body.role : 'admin',
                first_name: (body.first_name) ? body.first_name : '',
                last_name: (body.last_name) ? body.last_name : '',
                username: (body.username) ? body.username : 'anybody',
                display_name: (body.display_name) ? body.display_name : '',
                phone: (body.phone) ? body.phone : '',
                avatar: (body.avatar) ? body.avatar : '',
                company: (body.company) ? body.company : '',
                status: true
            });
            const newAdmin = await Admins.create(admin);
            if (!_.isEmpty(newAdmin)) {
                delete newAdmin.password;
                return res.status(201).json({
                    user: newAdmin
                });
            } else {
                return res.status(500).json({
                    message: 'Error insert admin'
                });
            }
        });
    }

    public async counts(req: Request, res: Response) {
        const params = req['query'] ? req['query'] : {};
        const query = processQuery(params);
        const numberTotalAdmins = await Admins.countDocuments(query);
        if (numberTotalAdmins) {
            res.status(200).json({
                total: numberTotalAdmins
            });
        } else {
            res.status(200).json({
                total: 0
            });
        }
    }

    public async gets(req: Request, res: Response) {
        const params = req['query'] ? req['query'] : {};
        const query = processQuery(params);
        const options = processOptions(params);

        const admins = await Admins.aggregate([{
            $match: query
        }, {
            $project: {
                _id: 1,
                first_name: 1,
                last_name: 1,
                username: 1,
                email: 1,
                role: 1,
                display_name: 1,
                company: 1,
                address: 1,
                phone: 1,
                avatar: 1,
                status: 1,
                updated_at: 1,
                created_at: 1
            }
        },
            {$sort: options.sort},
            {$skip: options.skip},
            {$limit: options.limit}
        ]);
        if (admins) {
            res.status(200).json({
                data: admins,
                count: admins.length
            });
        } else {
            res.status(404).json({
                message: 'Admin not found'
            });
        }
    }

    public async getById(req: Request, res: Response) {
        const params = req['params'] ? req['params'] : {};
        if(params.admin_id) {
            const admin = await Admins.findOne({_id: params.admin_id});
            if(!_.isEmpty(admin)) {
                delete admin.password;
                res.status(200).json({
                    user: admin
                });
            } else {
                res.status(404).json({
                    message: 'Admin not found'
                });
            }
        } else {
            res.status(400).json({
                message: 'ID invalid'
            });
        }
    }

    public async edit(req: Request, res: Response) {
        const id = req.params.admin_id;
        const body = req['body'] ? req['body'] : {};
        const adminOld = await Admins.findOne({_id: id});
        if (adminOld) {
            const admin = {
                email: (body.email) ? body.email : adminOld.email,
                username: (body.username) ? body.username : adminOld.username,
                first_name: (body.first_name) ? body.first_name : adminOld.first_name,
                last_name: (body.last_name) ? body.last_name : adminOld.last_name,
                display_name: (body.display_name) ? body.display_name : adminOld.display_name,
                company: (body.company) ? body.company : adminOld.company,
                phone: (body.phone) ? body.phone : adminOld.phone,
                avatar: (body.avatar) ? body.avatar : adminOld.avatar,
                status: body.status ? body.status : adminOld.status
            };
            const resultUpdate = await Admins.updateOne({_id: adminOld._id}, {
                $set: admin
            }, {
                runValidators: true
            });
            if (resultUpdate && resultUpdate.ok) {
                res.status(200).json({
                    user_id: adminOld._id
                });
            } else {
                res.status(500).json({
                    message: 'Update database ERROR'
                });
            }
        } else {
            res.status(404).json({
                message: 'Admin not found'
            });
        }
    }

    public async login (req: Request, res: Response) {
        const body = req['body'] ? req['body'] : {};
        const admin = await Admins.findOne({ email: body.email });
        if (!_.isEmpty(admin)) {
            bcrypt.compare(body.password, admin.password, async (err, result) => {
                if (err) {
                    res.status(401).json({
                        message: "Authentication failed. Wrong password."
                    });
                }
                if (result) {
                    let adminPer = await Roles.findOne({name: admin.role})
                        .select('permissions');
                    const token = jwt.sign({
                            email: admin.email,
                            userId: admin._id,
                            role: admin.role,
                            permissions: adminPer.permissions
                        },
                        config.JWT_KEY,
                        {
                            expiresIn: "24h"
                        }
                    );
                    admin.password = undefined;
                    let expire = new Date();
                    expire.setTime(expire.getTime() + (1*24*60*60*1000));
                    res.status(200).json({
                        token: token,
                        expire_time: expire.toUTCString(),
                        user: admin
                    });
                } else {
                    res.status(401).json({
                        message: "Unauthorized"
                    });
                }
            });
        } else {
            res.status(404).json({
                message: 'Admin not found'
            });
        }
    }

    public async delete(req: Request, res: Response) {
        if(req.params.admin_id) {
            const admin = await Admins.remove({ _id: req.params.admin_id });
            if (admin.ok) {
                res.status(200).json({
                    message: "Admin deleted"
                });
            } else {
                res.status(401).json({
                    message: "Admin deleted FAIL"
                });
            }
        } else {
            res.status(400).json({
                message: 'ID invalid'
            })
        }
    }
}