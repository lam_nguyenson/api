import * as _ from "lodash";
import { Request, Response, NextFunction } from 'express';
import {Roles} from '../models/role';
import {processOptions, processQuery} from '../core/utils';

export class RolesController {
    
    public async gets(req: Request, res: Response) {
        const params = req['query'] ? req['query'] : {};
        const query = processQuery(params);
        const options = processOptions(params);
        const roles = await Roles.aggregate([{
            $match: query
        }, {
            $project: {
                _id: 1,
                name: 1,
                permission: 1,
                status: 1,
                updated_at: 1,
                created_at: 1
            }
        },
            {$sort: options.sort},
            {$skip: options.skip},
            {$limit: options.limit}
        ]);
        if (roles) {
            res.status(200).json({
                data: roles,
                count: roles.length
            });
        } else {
            res.status(404).json({
                message: 'Roles not found'
            });
        }
    }

    public async add(req: Request, res: Response) {
        if(!req.body.name) {
            res.status(400).json({
                message: 'Role name invalid'
            })
        }
        const role = await Roles.findOne({'name': req.body.name});
        if(role) {
            res.status(400).json({
                message: 'Role name already exist'
            });
        } else {
            let {name, permissions} = req.body;
            const newRole = new Roles({
                name: name,
                permissions: permissions,
                status: true
            });
            if(Array.isArray(permissions)) {
                const result = await Roles.create(newRole);
                if (!_.isEmpty(result)) {
                    res.status(201).json({
                        role_id: result._id
                    });
                } else {
                    res.status(500).json({
                        message: 'Insert Role Fail'
                    });
                }
            } else {
                return res.status(400).json({
                    message: 'Permissions request invalid'
                });
            }
        }
    }

    public async getById(req: Request, res: Response) {
        if (req.params.role_id) {
            const role = await Roles.findOne({_id: req.params.role_id});
            if(role) {
                res.status(200).json({
                    role: role
                });
            } else {
                res.status(404).json({
                    message: 'Role not found'
                });
            }
        } else {
            res.status(401).json({
                message: 'ID not found'
            });
        }

    }

    public async edit(req: Request, res: Response) {
        const id = req.body.role_id;
        if(!req.body.name) {
            res.status(400).json({
                message: 'Role name invalid'
            })
        } else {
            const result = await Roles.findOneAndUpdate({ _id: id }, {
                    name: req.body.name,
                    permissions: req.body.permissions
                }, {
                    new: true,
                    runValidators: true
                });
            if (result) {
                res.status(200).json({
                    role: result
                });
            } else {
                res.status(500).json({
                    message: 'Update role Fail'
                });
            }
        }
    }

    public async delete(req: Request, res: Response) {
        const result = await Roles.remove({_id: req.body.role_id});
        if (result.ok) {
            res.status(200).json({
                message: 'Deleted Role Success'
            });
        } else {
            res.status(500).json({
                message: 'Deleted Role Fail'
            });
        }
    }
}