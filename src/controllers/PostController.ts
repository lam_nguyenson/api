import * as _ from "lodash";
import { Request, Response, NextFunction } from 'express';
import {Posts} from '../models/post';
import * as mongoose from 'mongoose';
import {processOptions, processQuery} from '../core/utils';

export class PostController {

    public async counts(req: Request, res: Response, next: NextFunction) {
        const params = req['query'] ? req['query'] : {};
        const query = processQuery(params);
        const numberTotalPosts = await Posts.countDocuments(query);
        if (numberTotalPosts) {
            res.status(200).json({
                total: numberTotalPosts
            });
        } else {
            res.status(200).json({
                total: 0
            });
        }
    }

    public async gets(req: Request, res: Response, next: NextFunction) {
        const params = req['query'] ? req['query'] : {};
        const query = processQuery(params);
        const options = processOptions(params);
        const posts = await Posts.aggregate([{
            $match: query
        }, {
            $project: {
                _id: 1,
                user: 1,
                comment_ids: 1,
                cats: 1,
                name: 1,
                title: 1,
                content: 1,
                image_features: 1,
                image_gallerys: 1,
                excerpt: 1,
                feature: 1,
                slug: 1,
                tags: 1,
                status: 1,
                updated_at: 1,
                created_at: 1
            }
        },
            {$sort: options.sort},
            {$skip: options.skip},
            {$limit: options.limit}
        ]);
        if (posts) {
            res.status(200).json({
                data: posts
            });
        } else {
            res.status(404).json({
                message: 'Posts not found'
            });
        }
    }

    public async add(req: Request, res: Response) {
        const body = req['body'] ? req['body'] : {};
        if (body && body.user && body.user.user_id) {
            const post = new Posts({
                _id: new mongoose.Types.ObjectId(),
                user: body.user ? body.user : {},
                comment_ids: body.comment_ids ? body.comment_ids : [],
                cats: body.cats ? body.cats : [],
                name: body.name ? body.name : '',
                title: body.title ? body.title : '',
                content: body.content ? body.content : '',
                image_features: body.image_features ? body.image_features : '',
                excerpt: body.excerpt ? body.excerpt : '',
                feature: body.feature ? body.feature : false,
                slug: body.slug ? body.slug : '',
                tags: body.tags ? body.tags : [],
                status: true,
            });

            const resultCreate = await Posts.create(post);
            if (resultCreate) {
                res.status(201).json({
                    post: resultCreate
                });
            } else {
                res.status(500).json({
                    message: 'Fail insert Post'
                });
            }
        }
    }

    public async getById(req: Request, res: Response, next: NextFunction) {
        const id = req.params.post_id;
        if (id) {
            const post = await Posts.findOne({_id: id});
            if (post) {
                res.status(200).json({
                    post: post
                });
            } else {
                res.status(404).json({
                    message: 'Posts not found'
                });
            }
        } else {
            res.status(500).json({
                message: 'ID Invalid'
            });
        }

    }


    public async edit(req: Request, res: Response, next: NextFunction) {
        const id = req.params.post_id;
        const body = req['body'] ? req['body'] : {};

        const oldPost = await Posts.findOne({_id: id});
        if (oldPost)  {
            const post = {
                user: body.user ? body.user : oldPost.user,
                comment_ids: body.comment_ids ? body.comment_ids : oldPost.comment_ids,
                cats: body.cats ? body.cats : oldPost.cats,
                name: body.name ? body.name : oldPost.name,
                title: body.title ? body.title : oldPost.title,
                content: body.content ? body.content : oldPost.content,
                image_features: body.image_features ? body.image_features : oldPost.image_features,
                excerpt: body.excerpt ? body.excerpt : oldPost.excerpt,
                feature: body.feature ? body.feature : oldPost.feature,
                slug: body.slug ? body.slug : oldPost.slug,
                tags: body.tags ? body.tags : oldPost.tags,
            };

            const result = await Posts.updateOne({_id: oldPost._id}, {
                $set: post
            });
            if (result && result.ok) {
                res.status(200).json({
                    post_id: oldPost._id
                });
            } else {
                res.status(500).json({
                    message: 'Fail update Post'
                });
            }
        } else {
            res.status(404).json({
                message: 'Post not found'
            });
        }
    }

    public async delete(req: Request, res: Response, next: NextFunction) {
        const result = await Posts.remove({_id: req.body.post_id});
        if (result.ok) {
            res.status(200).json({
                message: 'Deleted Post Success'
            });
        } else {
            res.status(500).json({
                message: 'Deleted Post Fail'
            });
        }
    }
}