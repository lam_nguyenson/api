import * as _ from "lodash";
import { Request, Response, NextFunction } from 'express';
import {Comments} from '../models/comment';
import * as mongoose from 'mongoose';
import {Posts} from '../models/post';
import {processOptions, processQuery} from '../core/utils';

export class CommentController {

    public async counts(req: Request, res: Response, next: NextFunction) {
        const params = req['query'] ? req['query'] : {};
        const query = processQuery(params);

        const numberTotalComments = await Comments.countDocuments(query);
        if (numberTotalComments) {
            res.status(200).json({
                total: numberTotalComments
            });
        } else {
            res.status(200).json({
                total: 0
            });
        }
    }

    public async gets(req: Request, res: Response, next: NextFunction) {
        const params = req['query'] ? req['query'] : {};
        const query = processQuery(params);
        const options = processOptions(params);
        const comments = await Comments.aggregate([{
            $match: query
        }, {
            $project: {
                _id: 1,
                post_id: 1,
                author: 1,
                author_email: 1,
                content: 1,
                approved: 1,
                type: 1,
                parent: 1,
                updated_at: 1,
                created_at: 1
            }
        },
            {$sort: options.sort},
            {$skip: options.skip},
            {$limit: options.limit}
        ]);
        if (comments) {
            res.status(200).json({
                data: comments
            });
        } else {
            res.status(404).json({
                message: 'Comments not found'
            });
        }
    }

    public async add(req: Request, res: Response, next: NextFunction) {
        const body = req['body'] ? req['body'] : {};
        if (body && body.post_id) {
            const comment = new Comments({
                _id: new mongoose.Types.ObjectId(),
                post_id: body.post_id,
                author: body.author ? body.author : '',
                author_email: body.author_email ? body.author_email : '',
                content: body.content ? body.content : '',
                approved: body.approved ? body.approved : false,
                type: body.type ? body.type : '',
                parent: body.parent ? body.parent : '',
            });

            const commentResult = await  Comments.create(comment);
            if (commentResult && commentResult._id) {
                const resultUpdate = await Posts.updateOne({_id: commentResult.post_id }, {
                    $push: { commentIds: commentResult._id }
                });
                if (resultUpdate) {
                    res.status(200).json({
                        comment: commentResult
                    });
                } else {
                    res.status(404).json({
                        message: 'Post not found'
                    });
                }
            } else {
                res.status(500).json({
                    message: 'Create a comment FAIL'
                });
            }
        }
    }

    public async getById(req: Request, res: Response, next: NextFunction) {
        const id = req.params.comment_id;
        if (id) {
            const comment = await Comments.findOne({_id: id});
            if (comment) {
                res.status(200).json({
                    comment: comment
                });
            } else {
                res.status(404).json({
                    message: 'Comment not found'
                });
            }
        } else {
            res.status(500).json({
                message: 'ID Invalid'
            });
        }

    }

    public async edit(req: Request, res: Response, next: NextFunction) {
        const id = req.params.comment_id;
        const body = req['body'] ? req['body'] : {};

        const oldComment = await Comments.findOne({_id: id});
        if (oldComment)  {
            const comment = {
                post_id: body.post_id,
                author: body.author ? body.author : oldComment.author,
                author_email: body.author_email ? body.author_email : oldComment.author_email,
                content: body.content ? body.content : oldComment.content,
                approved: body.approved ? body.approved : oldComment.approved,
                type: body.type ? body.type : oldComment.type,
                parent: body.parent ? body.parent : oldComment.parent,
            };

            const result = await Comments.updateOne({_id: oldComment._id}, {
                $set: comment
            });
            if (result && result.ok) {
                res.status(200).json({
                    comment_id: oldComment._id
                });
            } else {
                res.status(500).json({
                    message: 'Fail update Comment'
                });
            }
        } else {
            res.status(404).json({
                message: 'Comment not found'
            });
        }
    }

    public async delete(req: Request, res: Response, next: NextFunction) {
        const result = await Comments.remove({_id: req.body.comment_id});
        if (result.ok) {
            res.status(200).json({
                message: 'Deleted Comment Success'
            });
        } else {
            res.status(500).json({
                message: 'Deleted Comment Fail'
            });
        }
    }
}