import * as _ from "lodash";
import { Request, Response, NextFunction } from 'express';
import * as bcrypt from 'bcrypt';
import * as jwt from 'jsonwebtoken';
import * as mongoose from 'mongoose';
import { Users } from './../models/user';
import { config } from './../../envs/config';
import {processOptions, processQuery} from '../core/utils';

export class UserController {
    public async add(req: Request, res: Response) {
        const body = req['body'] ? req['body'] : {};
        if(!body.email || !body.password) {
            res.status(400).json({
                message: (!body.email) ? 'Email invalid' : 'Password invalid'
            });
        }
        const user = await Users.findOne({$or: [{email: body.email}, {username: body.username}]});
        if(!_.isEmpty(user)) {
            return res.status(409).json({
                message: 'Mail exits'
            });
        }

        bcrypt.hash(body.password, 10, async (err, hash) => {
            if(err) {
                return res.status(500).json({
                    message: 'Interal server error'
                });
            }
            const user = new Users({
                _id: new mongoose.Types.ObjectId(),
                email: body.email,
                password: hash,
                role: (body.role) ? body.role : 'guest',
                first_name: (body.first_name) ? body.first_name : '',
                last_name: (body.last_name) ? body.last_name : '',
                username: (body.username) ? body.username : 'anybody',
                display_name: (body.display_name) ? body.display_name : '',
                phone: (body.phone) ? body.phone : '',
                avatar: (body.avatar) ? body.avatar : '',
                company: (body.company) ? body.company : '',
                status: true
            });
            const newUser = await Users.create(user);
            if (!_.isEmpty(newUser)) {
                delete newUser.password;
                return res.status(201).json({
                    user: newUser
                });
            } else {
                return res.status(500).json({
                    message: 'Error insert user'
                });
            }
        });
    }

    public async counts(req: Request, res: Response) {
        const params = req['query'] ? req['query'] : {};
        const query = processQuery(params);
        const numberTotalUsers = await Users.countDocuments(query);
        if (numberTotalUsers) {
            res.status(200).json({
                total: numberTotalUsers
            });
        } else {
            res.status(200).json({
                total: 0
            });
        }
    }

    public async gets(req: Request, res: Response) {
        const params = req['query'] ? req['query'] : {};
        const query = processQuery(params);
        const options = processOptions(params);
        const users = await Users.aggregate([{
            $match: query
        }, {
            $project: {
                _id: 1,
                first_name: 1,
                last_name: 1,
                username: 1,
                email: 1,
                role: 1,
                display_name: 1,
                company: 1,
                address: 1,
                phone: 1,
                avatar: 1,
                status: 1,
                updated_at: 1,
                created_at: 1
            }
        },
            {$sort: options.sort},
            {$skip: options.skip},
            {$limit: options.limit}
        ]);
        if (users) {
            res.status(200).json({
                data: users,
                count: users.length
            });
        } else {
            res.status(404).json({
                message: 'Admin not found'
            });
        }
    }

    public async getById(req: Request, res: Response) {
        const params = req['params'] ? req['params'] : {};
        if(params.user_id) {
            const user = await Users.findOne({_id: params.user_id});
            if(!_.isEmpty(user)) {
                delete user.password;
                res.status(200).json({
                    user: user
                });
            } else {
                res.status(404).json({
                    message: 'Admin not found'
                });
            }
        } else {
            res.status(400).json({
                message: 'ID invalid'
            });
        }
    }

    public async edit(req: Request, res: Response) {
        const id = req.params.user_id;
        const body = req['body'] ? req['body'] : {};
        const userOld = await Users.findOne({_id: id});
        if (userOld) {
            const user = {
                email: (body.email) ? body.email : userOld.email,
                username: (body.username) ? body.username : userOld.username,
                first_name: (body.first_name) ? body.first_name : userOld.first_name,
                last_name: (body.last_name) ? body.last_name : userOld.last_name,
                display_name: (body.display_name) ? body.display_name : userOld.display_name,
                company: (body.company) ? body.company : userOld.company,
                phone: (body.phone) ? body.phone : userOld.phone,
                avatar: (body.avatar) ? body.avatar : userOld.avatar,
                status: body.status ? body.status : userOld.status
            };
            const resultUpdate = await Users.updateOne({_id: userOld._id}, {
                $set: user
            }, {
                runValidators: true
            });
            if (resultUpdate && resultUpdate.ok) {
                res.status(200).json({
                    user_id: userOld._id
                });
            } else {
                res.status(500).json({
                    message: 'Update database ERROR'
                });
            }
        } else {
            res.status(404).json({
                message: 'User not found'
            });
        }
    }

    public async login (req: Request, res: Response) {
        const body = req['body'] ? req['body'] : {};
        const user = await Users.findOne({ email: body.email });
        if (!_.isEmpty(user)) {
            bcrypt.compare(body.password, user.password, async (err, result) => {
                if (err) {
                    res.status(401).json({
                        message: "Authentication failed. Wrong password."
                    });
                }
                if (result) {
                    const token = jwt.sign({
                            email: user.email,
                            userId: user._id,
                            role: user.role
                        },
                        config.JWT_KEY,
                        {
                            expiresIn: "24h"
                        }
                    );
                    user.password = undefined;
                    let expire = new Date();
                    expire.setTime(expire.getTime() + (1*24*60*60*1000));
                    res.status(200).json({
                        token: token,
                        expire_time: expire.toUTCString(),
                        user: user
                    });
                } else {
                    res.status(401).json({
                        message: "Unauthorized"
                    });
                }
            });
        } else {
            res.status(404).json({
                message: 'Admin not found'
            });
        }
    }

    public async delete(req: Request, res: Response) {
        if(req.params.user_id) {
            const user = await Users.remove({ _id: req.params.user_id });
            if (user.ok) {
                res.status(200).json({
                    message: "User deleted"
                });
            } else {
                res.status(401).json({
                    message: "User deleted FAIL"
                });
            }
        } else {
            res.status(400).json({
                message: 'ID invalid'
            })
        }
    }
}