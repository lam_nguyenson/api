import * as mongoose from 'mongoose';
import * as _ from "lodash";
import { Request, Response, NextFunction } from 'express';
import {Medias, TypeMedia} from '../models/media';
import {formatNameImageResize} from '../core/MediaUpload';
const sharp = require('sharp');

export class MediaController {
    public getById(req: Request, res: Response, next: NextFunction): void {
        Medias.findOne({_id: req.params.media_id}).exec().then(result => {
            if (result) {
                res.status(201).json({
                    media: {
                        _id: result._id,
                        url: result.url,
                        size: result.size
                    }
                });
            } else {
                res.status(404).json({
                    message: 'Not found'
                });
            }
        }).catch(err => {
            res.status(500).json({
                message: err
            });
        });
    }

    public getQueries(req: Request, res: Response, next: NextFunction): void {
        const body = req['body'] ? req['body'] : {};
        let query = {};
        let options = {};
        if (body) {
            query = body['query'] ? body['query'] : {};
            options = body['options'] ? body['options'] : {};
        }
        Medias.aggregate([{
            $match: query
        }, {
            $project: {
                _id: 1,
                url: 1,
                size: 1
            }
        }]).exec().then(result => {
            if (result && result.length > 0) {
                res.status(200).json({
                    data: result,
                    count: result.length
                })
            } else {
                res.status(404).json({
                    message: 'Not found'
                });
            }
        }).catch(err => {
            res.status(500).json({error: err});
        });
    }

    public uploads(req: Request, res: Response, next: NextFunction): void {
        const files = req['files'] ? req['files'] : '';
        const body = req['body'] ? req['body'] : '';
        if (files && (files.length > 0) && body) {
            const medias = [];
            const urlHost = req.protocol + '://' + req.get('host') + '/uploads';
            _.each(files, (file: any) => {
                const pathSplit =  _.split(file.path, 'uploads');
                const url =  urlHost + _.last(pathSplit);
                const media = new Medias({
                    _id: new mongoose.Types.ObjectId(),
                    user_id: body.user_id ? body.user_id : '',
                    name: body.name ? body.name : '',
                    original_name: file.originalname ? file.originalname : '',
                    file_name: file.filename ? file.filename : '',
                    encoding: file.encoding ? file.encoding : '',
                    mimetype: file.mimetype ? file.mimetype : '',
                    path: file.path ? file.path : '',
                    url: url,
                    type: file.type ? file.type : TypeMedia.Others
                });
                medias.push(media);
            });
            Medias.create(medias).then(results => {
                const mediaResults = [];
                _.each(results, (result: any) => {
                   const newMedia = {
                       _id: result._id,
                       url: result.url,
                       size: result.size
                   };
                   mediaResults.push(newMedia);
                });
                res.status(201).json({
                    medias: mediaResults
                });
            }).catch((err) => {
                res.status(500).json({
                    message: err
                });
            })
        } else {
            res.status(500).json({
                message: 'Data in body not found '
            });
        }
    }

    public upload(req: Request, res: Response, next: NextFunction): void {
        const file = req['file'] ? req['file'] : '';
        const body = req['body'] ? req['body'] : '';
        if (file && body) {
            const urlHost = req.protocol + '://' + req.get('host') + '/uploads';
            let size = {
                width: 0,
                height: 0
            };
            if ((file.type === TypeMedia.Image) && body.width && body.height) {
                size.width = Number(body.width);
                size.height = Number(body.height);
                const nameFormat = formatNameImageResize(file.path, size.width, size.height);
                sharp(file.path)
                    .resize(size.width, size.height)
                    .toFile(nameFormat).then((result: any) => {
                    if (result) {
                        const pathSplit =  _.split(nameFormat, 'uploads');
                        const url = urlHost + _.last(pathSplit);
                        const media = new Medias({
                            _id: new mongoose.Types.ObjectId(),
                            user_id: body.user_id ? body.user_id : '',
                            name: body.name ? body.name : '',
                            original_name: file.originalname ? file.originalname : '',
                            file_name: file.filename ? file.filename : '',
                            encoding: file.encoding ? file.encoding : '',
                            mimetype: file.mimetype ? file.mimetype : '',
                            path: file.path ? file.path : '',
                            url: url,
                            size: size,
                            type: file.type ? file.type : TypeMedia.Others
                        });
                        Medias.create(media).then(result => {
                            res.status(201).json({
                                media: {
                                    _id: result._id,
                                    url: result.url,
                                    size: result.size
                                }
                            });
                        }).catch((err) => {
                            res.status(500).json({
                                message: err
                            });
                        })
                    } else {
                        res.status(500).json({
                            message: 'Data in body not found '
                        });
                    }
                }).catch(err => {
                    res.status(500).json({
                        message: err
                    });
                });
            } else {
                const pathSplit =  _.split(file.path, 'uploads');
                const url = urlHost + _.last(pathSplit);
                const media = new Medias({
                    _id: new mongoose.Types.ObjectId(),
                    user_id: body.user_id ? body.user_id : '',
                    name: body.name ? body.name : '',
                    original_name: file.originalname ? file.originalname : '',
                    file_name: file.filename ? file.filename : '',
                    encoding: file.encoding ? file.encoding : '',
                    mimetype: file.mimetype ? file.mimetype : '',
                    path: file.path ? file.path : '',
                    url: url,
                    size: size,
                    type: file.type ? file.type : TypeMedia.Others
                });
                Medias.create(media).then(result => {
                    res.status(201).json({
                        media: {
                            _id: result._id,
                            url: result.url,
                            size: result.size
                        }
                    });
                }).catch((err) => {
                    res.status(500).json({
                        message: err
                    });
                })
            }
        } else {
            res.status(500).json({
                message: 'Data in body not found '
            });
        }
    }

    public edit(req: Request, res: Response, next: NextFunction) {
        const id = req.params.media_id;
        const file = req['file'] ? req['file'] : '';
        const body = req['body'] ? req['body'] : {};
        Medias.findOne({_id: id}).exec()
            .then((result: any) => {
                if (result)  {
                    const pathSplit =  _.split(file.path, 'uploads');
                    const url = req.protocol + '://' + req.get('host') + '/uploads' + _.last(pathSplit);
                    const media = {
                        user_id: body.user_id ? body.user_id : result.user_id,
                        name: body.name ? body.name : result.name,
                        original_name: file.originalname ? file.originalname : result.original_name,
                        file_name: file.filename ? file.filename : result.file_name,
                        encoding: file.encoding ? file.encoding : result.encoding,
                        mimetype: file.mimetype ? file.mimetype : result.mimetype,
                        path: file.path ? file.path : result.path,
                        url: url ? url : result.url,
                        size: file.size ? file.size : result.size,
                        type: file.type ? file.type : result.type
                    };
                    Medias.updateOne({_id: result._id}, {
                        $set: media
                    }).exec().then((resultUpdate: any) => {
                        if (resultUpdate && resultUpdate.ok) {
                            res.status(200).json({
                                media_id: result._id
                            });
                        } else {
                            res.status(404).json({
                                message: 'Media not found'
                            });
                        }

                    }).catch(err => {
                        res.status(500).json({
                            message: err
                        });
                    });
                } else {
                    res.status(404).json({
                        message: 'Media not found'
                    });
                }
            }).catch(err => {
            res.status(500).json({
                message: err
            });
        });
    }

    public delete(req: Request, res: Response, next: NextFunction) {
    }
}