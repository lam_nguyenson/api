import { Request, Response, NextFunction } from 'express';
import {Permissions} from '../models/permission';

export class PermissionController {
    
    public getPermissions(req: Request, res: Response) {
        Permissions.find().exec().then(result => {
            res.status(200).json({
                data: result,
                count: result.length
            })
        }).catch(err => {
            res.status(500).json({error: err});
        });
    }
}